
Descripción de las capas de la apliación.

*Models: Clases encargadas de definir la estructura de los items visualizados en la aplicación: Series y películas.

*Presenters:  Clases intermediarias entre la vista y los servicios que se encargan de traer la data desde la API de TMDB. Un presenter y un cargador asíncrono de los servicios se encuentran aquí.

*Services: Clases encargadas de traer los datos de las series y películas desde la API de TMDB.

*Utils: Clases de uso general. Excepciones personalizadas y valores constantes requeridos en los diferentes paquetes de la aplicación.

*Views: La MainActivity, fragmentos y otras clases concernientes a la parte visual de la aplicación.


1. En qué consiste el principio de responsabilidad única? Cuál es su propósito?
RTA: En que cada clase, método y variable debe cumplir con solo un tarea. 
Esto se hace de esta manera para evitar la sobrecarga de responsabilidades a una sola entidad
y dificultar el futuro mantenimiento y escalabilidad de la aplicación.


2. Qué características tiene, según su opinión, un “buen” código o código limpio?
RTA: Tener alta cohesión y bajo acoplamiento. Y ser autodescriptivo al ser leido.