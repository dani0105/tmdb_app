package com.rappi.applistapeliculas;

import android.util.Log;

import com.rappi.applistapeliculas.models.ListasPeliculas;
import com.rappi.applistapeliculas.models.ListasSeries;
import com.rappi.applistapeliculas.services.apirest.IServicioPeliculas;
import com.rappi.applistapeliculas.services.apirest.IServicioSeries;
import com.rappi.applistapeliculas.services.apirest.impl.ServicioPeliculas;
import com.rappi.applistapeliculas.services.apirest.impl.ServicioSeries;
import com.rappi.applistapeliculas.utils.exceptions.ConexionFallidaException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest({Log.class})
public class ServiciosTest {


    static IServicioPeliculas servicioPeliculas;
    static ListasPeliculas listasPeliculas;
    static IServicioSeries servicioSeries;
    static ListasSeries listasSeries;

    @BeforeClass
    public static void setUp(){
        PowerMockito.mockStatic(Log.class);
        servicioPeliculas = new ServicioPeliculas();
        listasPeliculas = ListasPeliculas.getInstancia();
        servicioSeries = new ServicioSeries();
        listasSeries = ListasSeries.getInstancia();
    }

    @Test
    public void debeComprobarQueTraeElListadoDePeliculasPopulares(){

        try{
            this.servicioPeliculas.cargarListadoPeliculasPopulares();
            Thread.sleep(20000);//Se coloca este hilo para permitir que el cliente retrofit pueda realizar la consulta
                                        // en la API de peliculas. Si no se coloca este delay, la prueba sale erronea
            Assert.assertFalse(listasPeliculas.getPeliculasPopular().isEmpty());
            //Log.i("Peliculas populares", listasPeliculas.getPeliculasPopular().toString());
        }
        catch(ConexionFallidaException e){
            e.imprimirMensajeDeError();
            Assert.fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void debeComprobarQueTraeElListadoDePeliculasTopRated(){

        try{
            this.servicioPeliculas.cargarListadoPeliculasTopRated();
            Thread.sleep(20000);
            Assert.assertFalse(listasPeliculas.getPeliculasTopRated().isEmpty());
            //Log.i("Peliculas Top rated", listasPeliculas.getPeliculasTopRated().toString());
        }
        catch(ConexionFallidaException e){
            e.imprimirMensajeDeError();
            Assert.fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void debeComprobarQueTraeElListadoDePeliculasUpcoming(){

        try{
            this.servicioPeliculas.cargarListadoPeliculasUpcoming();
            Thread.sleep(20000);
            Assert.assertFalse(listasPeliculas.getPeliculasUpcoming().isEmpty());
            //Log.i("Peliculas Upcoming", listasPeliculas.getPeliculasUpcoming().toString());
        }
        catch(ConexionFallidaException e){
            e.imprimirMensajeDeError();
            Assert.fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void debeComprobarQueTraeElListadoDeSeriesPopulares(){

        try{
            this.servicioSeries.cargarListadoSeriesPopulares();
            Thread.sleep(20000);//Se coloca este hilo para permitir que el cliente retrofit pueda realizar la consulta
            // en la API de peliculas. Si no se coloca este delay, la prueba sale erronea
            Assert.assertFalse(listasSeries.getSeriesPopular().isEmpty());
        }
        catch(ConexionFallidaException e){
            e.imprimirMensajeDeError();
            Assert.fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void debeComprobarQueTraeElListadoDeSeriesTopRated(){

        try{
            this.servicioSeries.cargarListadoSeriesTopRated();
            Thread.sleep(20000);
            Assert.assertFalse(listasSeries.getSeriesTopRated().isEmpty());
        }
        catch(ConexionFallidaException e){
            e.imprimirMensajeDeError();
            Assert.fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
