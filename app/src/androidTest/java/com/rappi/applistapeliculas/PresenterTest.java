package com.rappi.applistapeliculas;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;

import com.rappi.applistapeliculas.models.pelicula.PPopular;
import com.rappi.applistapeliculas.models.pelicula.PTopRated;
import com.rappi.applistapeliculas.models.pelicula.PUpComing;
import com.rappi.applistapeliculas.models.serie.SPopular;
import com.rappi.applistapeliculas.models.serie.STopRated;
import com.rappi.applistapeliculas.presenters.IPresenterPeliculasYSeries;
import com.rappi.applistapeliculas.presenters.impl.PresenterPeliculasYSeries;
import com.rappi.applistapeliculas.utils.exceptions.ConexionFallidaException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.IOException;
import java.util.List;


@RunWith(AndroidJUnit4.class)
@MediumTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PresenterTest {

    private static IPresenterPeliculasYSeries presenterPeliculasYSeries;
    private List<PPopular> listaPPopular = null;
    private List<PTopRated> listaPTopRated = null;
    private List<PUpComing> listaPUpcoming = null;
    private List<SPopular> listaSPopular = null;
    private List<STopRated> listaSTopRated = null;
    private static Context instrumentationCtx;

    @BeforeClass
    public static void setUp(){
        //PowerMockito.mockStatic(Log.class);
        instrumentationCtx = InstrumentationRegistry.getContext();
        presenterPeliculasYSeries = PresenterPeliculasYSeries.getInstancia();
    }

    @Test
    public void AdebeComprobarQueRetornaElListadoDePeliculasPopulares(){
        try {
            listaPPopular = presenterPeliculasYSeries.getPeliculasPopularesDesdeLaAPIMadre();
            Thread.sleep(20000); //Para permitir el tiempo suficiente para la carga de los datos desde la API madre
            Assert.assertFalse(listaPPopular.isEmpty());
        } catch (ConexionFallidaException e) {
            Assert.fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void BdebeComprobarQueRetornaElListadoDePeliculasTopRated(){
        try {
            listaPTopRated = presenterPeliculasYSeries.getPeliculasTopRatedDesdeLaAPIMadre();
            Thread.sleep(20000); //Para permitir el tiempo suficiente para la carga de los datos desde la API madre
            Assert.assertFalse(listaPTopRated.isEmpty());
        } catch (ConexionFallidaException e) {
            Assert.fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void CdebeComprobarQueRetornaElListadoDePeliculasUpcoming(){
        try {
            listaPUpcoming = presenterPeliculasYSeries.getPeliculasUpComingDesdeLaAPIMadre();
            Thread.sleep(20000); //Para permitir el tiempo suficiente para la carga de los datos desde la API madre
            Assert.assertFalse(listaPUpcoming.isEmpty());
        } catch (ConexionFallidaException e) {
            Assert.fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void DdebeComprobarQueRetornaElListadoDeSeriesPopular(){
        try {
            listaSPopular = presenterPeliculasYSeries.getSeriesPopularesDesdeLaAPIMadre();
            Thread.sleep(20000); //Para permitir el tiempo suficiente para la carga de los datos desde la API madre
            Assert.assertFalse(listaSPopular.isEmpty());
        } catch (ConexionFallidaException e) {
            Assert.fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void EdebeComprobarQueRetornaElListadoDeSeriesTopRated(){
        try {
            listaSTopRated = presenterPeliculasYSeries.getSeriesTopRatedDesdeLaAPIMadre();
            Thread.sleep(20000); //Para permitir el tiempo suficiente para la carga de los datos desde la API madre
            Assert.assertFalse(listaSTopRated.isEmpty());
        } catch (ConexionFallidaException e) {
            Assert.fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    Prueba falla debido a permisos insuficientes al momento de ejecutar la app
    java.io.FileNotFoundException: ../file.ser (Permission denied)

    @Test
    public void FdebeComprobarQueSeAlmacenaTodaLaDataRetornadaEnCache(){
        List<PPopular> listaPPopular = null;
        List<PTopRated> listaPTopRated = null;
        List<PUpComing> listaPUpcoming = null;
        List<SPopular> listaSPopular = null;
        List<PTopRated> listaSTopRated = null;
        ((PresenterPeliculasYSeries)presenterPeliculasYSeries).setContext(instrumentationCtx);

        try {
            listaPPopular = presenterPeliculasYSeries.getPeliculasPopularesDesdeInternalStorage();
            listaPTopRated = presenterPeliculasYSeries.getPeliculasTopRatedDesdeInternalStorage();
            listaPUpcoming = presenterPeliculasYSeries.getPeliculasUpComingDesdeInternalStorage();
            listaSPopular = presenterPeliculasYSeries.getSeriesPopularesDesdeInternalStorage();
            listaSTopRated = presenterPeliculasYSeries.getSeriesTopRatedDesdeInternalStorage();
        } catch (IOException e) {
            //Assert.fail();
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            Assert.fail();
        }

        Assert.assertTrue(listaPPopular != null && !listaPPopular.isEmpty());
        Assert.assertTrue(listaPTopRated != null && !listaPTopRated.isEmpty());
        Assert.assertTrue(listaPUpcoming != null && !listaPUpcoming.isEmpty());
        Assert.assertTrue(listaSPopular != null && !listaSPopular.isEmpty());
        Assert.assertTrue(listaSTopRated != null && !listaPTopRated.isEmpty());

    }*/

}
