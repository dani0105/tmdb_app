package com.rappi.applistapeliculas.views.fragmentos;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.rappi.applistapeliculas.R;
import com.rappi.applistapeliculas.models.pelicula.PPopular;
import com.rappi.applistapeliculas.presenters.asynctask.CargadorDataAPI;
import com.rappi.applistapeliculas.views.adapters.RVAdapterPPopular;

import java.util.List;

public class FragmentoPPopular extends FragmentoGenerico {

    List<PPopular> listaPeliculasPopulares;

    public FragmentoPPopular(){
        super();
    }

    @Override
    public void setLista(List lista) {
        this.listaPeliculasPopulares = lista;
    }

    public List<PPopular> getListaPeliculasPopulares() {
        return listaPeliculasPopulares;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragmento_layout, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.titlePeliculasPopulares);
        CargadorDataAPI cargadorDataAPI = new CargadorDataAPI();
        cargadorDataAPI.setContext(getActivity());
        cargadorDataAPI.execute(this);
    }

    @Override
    public void cargarRV() {
        ProgressBar progressBar = (ProgressBar) getActivity().findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        setRvView((RecyclerView) getActivity().findViewById(R.id.rv));
        setRvAdapter(new RVAdapterPPopular());
        getRvAdapter().cargarData(listaPeliculasPopulares);
        getRvAdapter().notifyDataSetChanged();
        getRvView().setLayoutManager(new LinearLayoutManager(getActivity()));
    }

}
