package com.rappi.applistapeliculas.views.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.Filter;

import com.rappi.applistapeliculas.R;
import com.rappi.applistapeliculas.models.pelicula.PPopular;
import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RVAdapterPPopular extends RVAdapter {

    List<PPopular> peliculasPopularesFullList;
    List<PPopular> peliculasPopularesSearch;

    public RVAdapterPPopular() {
        super();
    }

    @Override
    public void clearContenido() {
        this.peliculasPopularesFullList.clear();
        this.peliculasPopularesSearch.clear();
    }

    @Override
    public void cargarData(List lista) {
        this.peliculasPopularesFullList = lista;
        this.peliculasPopularesSearch = new ArrayList<PPopular>(peliculasPopularesFullList);
    }

    @NonNull
    @Override
    public ContenidoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return super.onCreateViewHolder(viewGroup, i);
    }

    @Override
    public void onBindViewHolder(@NonNull ContenidoViewHolder contenidoViewHolder, int position) {
        super.onBindViewHolder(contenidoViewHolder, position);
        PPopular pPopular = this.peliculasPopularesSearch.get(position);
        contenidoViewHolder.tvTitle.setText(pPopular.getTitle());
        contenidoViewHolder.imageView.setImageResource(R.drawable.tmbd_iv);

        Picasso.get().load(ConstantesPeliculasYSeries.IMAGE_URL
                + pPopular.getPoster_path()).resize(400,300).into(contenidoViewHolder.imageView);
        contenidoViewHolder.tvReleaseDate.setText(pPopular.getRelease_date());
        contenidoViewHolder.tvOverview.setText(pPopular.getOverview());
    }

    @Override
    public int getItemCount() {
        return this.peliculasPopularesSearch.size();
    }

    @Override
    public Object obtenerValorOpcion(int posicion) {
        return this.peliculasPopularesSearch.get(posicion);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<PPopular> peliculasPopularesFilter = new ArrayList<PPopular>();
                if(constraint == null || constraint.length() == 0)
                    peliculasPopularesFilter.addAll(new ArrayList<PPopular>(peliculasPopularesFullList));
                else{
                    String filterPattern = constraint.toString().toLowerCase().trim();
                    for(PPopular pPopular : peliculasPopularesFullList){
                        if(pPopular.getTitle().toLowerCase().contains(filterPattern.toLowerCase())){
                            peliculasPopularesFilter.add(pPopular);
                        }
                    }
                }
                FilterResults results = new FilterResults();
                results.values = peliculasPopularesFilter;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                peliculasPopularesSearch.clear();
                peliculasPopularesSearch.addAll((List)results.values);
                notifyDataSetChanged();
            }
        };
    }
}
