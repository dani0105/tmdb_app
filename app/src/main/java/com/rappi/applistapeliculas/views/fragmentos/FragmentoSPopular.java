package com.rappi.applistapeliculas.views.fragmentos;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.rappi.applistapeliculas.R;
import com.rappi.applistapeliculas.models.serie.SPopular;
import com.rappi.applistapeliculas.presenters.asynctask.CargadorDataAPI;
import com.rappi.applistapeliculas.views.adapters.RVAdapterSPopular;

import java.util.List;

public class FragmentoSPopular extends FragmentoGenerico {

    List<SPopular> listaSeriesPopulares;

    public FragmentoSPopular(){
        super();
    }

    @Override
    public void setLista(List lista) {
        this.listaSeriesPopulares = lista;
    }

    @Override
    public void cargarRV() {
        ProgressBar progressBar = (ProgressBar) getActivity().findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        setRvView((RecyclerView) getActivity().findViewById(R.id.rv));
        setRvAdapter(new RVAdapterSPopular());
        getRvAdapter().cargarData(listaSeriesPopulares);
        getRvAdapter().notifyDataSetChanged();
        getRvView().setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragmento_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.titleSeriesPopulares);
        CargadorDataAPI cargadorDataAPI = new CargadorDataAPI();
        cargadorDataAPI.setContext(getActivity());
        cargadorDataAPI.execute(this);

    }
}
