package com.rappi.applistapeliculas.views.adapters;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.Filter;

import com.rappi.applistapeliculas.R;
import com.rappi.applistapeliculas.models.serie.SPopular;
import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RVAdapterSPopular extends RVAdapter {
    List<SPopular> seriesPopularesFullList;
    List<SPopular> seriesPopularesSearch;

    public RVAdapterSPopular() {
        super();
    }

    @Override
    public void clearContenido() {
        this.seriesPopularesFullList.clear();
        this.seriesPopularesSearch.clear();
    }

    @Override
    public void cargarData(List lista) {
        this.seriesPopularesFullList = lista;
        this.seriesPopularesSearch = new ArrayList<SPopular>(seriesPopularesFullList);
    }

    @NonNull
    @Override
    public ContenidoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return super.onCreateViewHolder(viewGroup, i);
    }

    @Override
    public void onBindViewHolder(@NonNull ContenidoViewHolder contenidoViewHolder, int position) {
        super.onBindViewHolder(contenidoViewHolder, position);
        SPopular sPopular = this.seriesPopularesSearch.get(position);
        contenidoViewHolder.tvTitle.setText(sPopular.getName());
        contenidoViewHolder.imageView.setImageResource(R.drawable.tmbd_iv);
        contenidoViewHolder.tvReleaseDate.setText(sPopular.getFirst_air_date());
        Picasso.get().load(ConstantesPeliculasYSeries.IMAGE_URL
                + sPopular.getPoster_path()).resize(400,300).into(contenidoViewHolder.imageView);
        contenidoViewHolder.tvOverview.setText(sPopular.getOverview());
    }

    @Override
    public int getItemCount() {
        return this.seriesPopularesSearch.size();
    }

    @Override
    public Object obtenerValorOpcion(int posicion) {
        return this.seriesPopularesSearch.get(posicion);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<SPopular> peliculasPopularesFilter = new ArrayList<SPopular>();
                if(constraint == null || constraint.length() == 0)
                    peliculasPopularesFilter.addAll(new ArrayList<SPopular>(seriesPopularesFullList));
                else{
                    String filterPattern = constraint.toString().toLowerCase().trim();
                    for(SPopular sPopular : seriesPopularesFullList){
                        if(sPopular.getName().toLowerCase().contains(filterPattern.toLowerCase())){
                            peliculasPopularesFilter.add(sPopular);
                        }
                    }
                }
                FilterResults results = new FilterResults();
                results.values = peliculasPopularesFilter;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                seriesPopularesSearch.clear();
                seriesPopularesSearch.addAll((List)results.values);
                notifyDataSetChanged();
            }
        };
    }
}
