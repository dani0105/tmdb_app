package com.rappi.applistapeliculas.views;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import com.rappi.applistapeliculas.R;
import com.rappi.applistapeliculas.views.fragmentos.FragmentoGenerico;
import com.rappi.applistapeliculas.views.fragmentos.FragmentoPPopular;
import com.rappi.applistapeliculas.views.fragmentos.FragmentoPTopRated;
import com.rappi.applistapeliculas.views.fragmentos.FragmentoPUpcoming;
import com.rappi.applistapeliculas.views.fragmentos.FragmentoSPopular;
import com.rappi.applistapeliculas.views.fragmentos.FragmentoSTopRated;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    FragmentoPPopular peliculasPopularFragment;
    FragmentoPTopRated peliculasTopRatedFragment;
    FragmentoPUpcoming peliculasUpComingFragment;
    FragmentoSPopular seriesPopularFragment;
    FragmentoSTopRated seriesTopRatedFragment;

    Toolbar toolbar;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inicializarToolbar();
        inicializarDrawerLayout();
        cargarFragmentoPorDefecto();

    }



    private void cargarFragmentoPorDefecto() {
        navigationView.getMenu().getItem(0).setChecked(true);
        fragmentManager = getSupportFragmentManager();
        peliculasPopularFragment = new FragmentoPPopular();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, peliculasPopularFragment);
        fragmentTransaction.commit();
    }


    public void inicializarToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_tmdb);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    private void inicializarDrawerLayout() {
        drawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer_layout);
        drawerToggle = getDrawerToggle();
        drawerLayout.setDrawerListener(drawerToggle);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        if (navigationView != null) {
            setupNavigationDrawerContent(navigationView);
        }

        setupNavigationDrawerContent(navigationView);

    }

    private ActionBarDrawerToggle getDrawerToggle() {
        return new ActionBarDrawerToggle(this, drawerLayout, 0, 0) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.navigation_drawer_menu, menu);
        menu.clear();
        getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        //searchItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItem.SHOW_AS_ACTION_IF_ROOM);

        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(getQueryTextListener());
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        return true;
    }

    private SearchView.OnQueryTextListener getQueryTextListener() {
        return new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                ((FragmentoGenerico)getVisibleFragment()).getRvAdapter().getFilter().filter(s);
                return true;
            }
        };
    }

    private Fragment getVisibleFragment() {
        List<Fragment> fragments = this.fragmentManager.getFragments();
        for (Fragment fragment : fragments) {
            if (fragment != null && fragment.isVisible())
                return fragment;
        }
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupNavigationDrawerContent(NavigationView navigationView) {
        this.navigationView = navigationView;
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.item_peliculas_populares:
                                menuItem.setChecked(true);
                                fragmentManager = getSupportFragmentManager();
                                fragmentTransaction = fragmentManager.beginTransaction();
                                if(peliculasPopularFragment == null)
                                    peliculasPopularFragment = new FragmentoPPopular();
                                fragmentTransaction.replace(R.id.fragment, peliculasPopularFragment);
                                fragmentTransaction.commit();
                                drawerLayout.closeDrawer(GravityCompat.START);
                            break;

                            case R.id.item_peliculas_top_rated:
                                menuItem.setChecked(true);
                                fragmentManager = getSupportFragmentManager();
                                fragmentTransaction = fragmentManager.beginTransaction();
                                if(peliculasTopRatedFragment == null)
                                    peliculasTopRatedFragment = new FragmentoPTopRated();
                                fragmentTransaction.replace(R.id.fragment, peliculasTopRatedFragment);
                                fragmentTransaction.commit();
                                drawerLayout.closeDrawer(GravityCompat.START);
                            break;

                            case R.id.item_peliculas_upcoming:
                                menuItem.setChecked(true);
                                fragmentManager = getSupportFragmentManager();
                                fragmentTransaction = fragmentManager.beginTransaction();
                                if(peliculasUpComingFragment == null)
                                    peliculasUpComingFragment = new FragmentoPUpcoming();
                                fragmentTransaction.replace(R.id.fragment, peliculasUpComingFragment);
                                fragmentTransaction.commit();
                                drawerLayout.closeDrawer(GravityCompat.START);
                            return true;

                            case R.id.item_series_populares:
                                menuItem.setChecked(true);
                                fragmentManager = getSupportFragmentManager();
                                fragmentTransaction = fragmentManager.beginTransaction();
                                if(seriesPopularFragment == null)
                                    seriesPopularFragment = new FragmentoSPopular();
                                fragmentTransaction.replace(R.id.fragment, seriesPopularFragment);
                                fragmentTransaction.commit();
                                drawerLayout.closeDrawer(GravityCompat.START);
                            return true;

                            case R.id.item_series_toprated:
                                menuItem.setChecked(true);
                                fragmentManager = getSupportFragmentManager();
                                fragmentTransaction = fragmentManager.beginTransaction();
                                if(seriesTopRatedFragment == null)
                                    seriesTopRatedFragment = new FragmentoSTopRated();
                                fragmentTransaction.replace(R.id.fragment, seriesTopRatedFragment);
                                fragmentTransaction.commit();
                                drawerLayout.closeDrawer(GravityCompat.START);
                            return true;
                        }
                        return true;
                    }
                });
    }

    /*
    public void setFragment(int position) {
        FragmentManager fragmentManager;
        FragmentTransaction fragmentTransaction;
        switch (position) {
            case 0:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                FragmentoPeliculas peliculasFragment = new FragmentoPeliculas();
                fragmentTransaction.replace(R.id.fragment, peliculasFragment);
                fragmentTransaction.commit();
                break;
            case 1:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                FragmentoSeries seriesFragment = new FragmentoSeries();
                fragmentTransaction.replace(R.id.fragment, seriesFragment);
                fragmentTransaction.commit();
                break;
        }
    }*/
}
