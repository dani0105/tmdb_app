package com.rappi.applistapeliculas.views.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.Filter;

import com.rappi.applistapeliculas.R;
import com.rappi.applistapeliculas.models.pelicula.PTopRated;
import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RVAdapterPTopRated extends RVAdapter {

    List<PTopRated> peliculasTopRatedFullList;
    List<PTopRated> peliculasTopRatedSearch;

    public RVAdapterPTopRated() {
        super();
    }

    @Override
    public void clearContenido() {
        this.peliculasTopRatedFullList.clear();
        this.peliculasTopRatedSearch.clear();
    }

    @Override
    public void cargarData(List lista) {
        this.peliculasTopRatedFullList = lista;
        this.peliculasTopRatedSearch = new ArrayList<PTopRated>(peliculasTopRatedFullList);
    }

    @NonNull
    @Override
    public ContenidoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return super.onCreateViewHolder(viewGroup, i);
    }

    @Override
    public void onBindViewHolder(@NonNull ContenidoViewHolder contenidoViewHolder, int position) {
        super.onBindViewHolder(contenidoViewHolder, position);
        PTopRated pTopRated = this.peliculasTopRatedSearch.get(position);
        contenidoViewHolder.tvTitle.setText(pTopRated.getTitle());
        contenidoViewHolder.imageView.setImageResource(R.drawable.tmbd_iv);
        contenidoViewHolder.tvReleaseDate.setText(pTopRated.getRelease_date());
        Picasso.get().load(ConstantesPeliculasYSeries.IMAGE_URL
                + pTopRated.getPoster_path()).resize(400,300).into(contenidoViewHolder.imageView);
        contenidoViewHolder.tvOverview.setText(pTopRated.getOverview());
    }

    @Override
    public int getItemCount() {
        return this.peliculasTopRatedSearch.size();
    }

    @Override
    public Object obtenerValorOpcion(int posicion) {
        return this.peliculasTopRatedSearch.get(posicion);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<PTopRated> peliculasTopRatedesFilter = new ArrayList<PTopRated>();
                if(constraint == null || constraint.length() == 0)
                    peliculasTopRatedesFilter.addAll(new ArrayList<PTopRated>(peliculasTopRatedFullList));
                else{
                    String filterPattern = constraint.toString().toLowerCase().trim();
                    for(PTopRated pTopRated : peliculasTopRatedFullList){
                        if(pTopRated.getTitle().toLowerCase().contains(filterPattern.toLowerCase())){
                            peliculasTopRatedesFilter.add(pTopRated);
                        }
                    }
                }
                FilterResults results = new FilterResults();
                results.values = peliculasTopRatedesFilter;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                peliculasTopRatedSearch.clear();
                peliculasTopRatedSearch.addAll((List)results.values);
                notifyDataSetChanged();
            }
        };
    }
}
