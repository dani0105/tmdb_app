package com.rappi.applistapeliculas.views.adapters;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.Filter;

import com.rappi.applistapeliculas.R;
import com.rappi.applistapeliculas.models.serie.STopRated;
import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RVAdapterSTopRated extends RVAdapter {

    List<STopRated> seriesTopRatedFullList;
    List<STopRated> seriesTopRatedSearch;

    public RVAdapterSTopRated() {
        super();
    }

    @Override
    public void clearContenido() {
        this.seriesTopRatedFullList.clear();
        this.seriesTopRatedSearch.clear();
    }

    @Override
    public void cargarData(List lista) {
        this.seriesTopRatedFullList = lista;
        this.seriesTopRatedSearch = new ArrayList<STopRated>(seriesTopRatedFullList);
    }

    @NonNull
    @Override
    public ContenidoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return super.onCreateViewHolder(viewGroup, i);
    }

    @Override
    public void onBindViewHolder(@NonNull ContenidoViewHolder contenidoViewHolder, int position) {
        super.onBindViewHolder(contenidoViewHolder, position);
        STopRated sPopular = this.seriesTopRatedSearch.get(position);
        contenidoViewHolder.tvTitle.setText(sPopular.getName());
        contenidoViewHolder.imageView.setImageResource(R.drawable.tmbd_iv);
        contenidoViewHolder.tvReleaseDate.setText(sPopular.getFirst_air_date());
        Picasso.get().load(ConstantesPeliculasYSeries.IMAGE_URL
                + sPopular.getPoster_path()).resize(400,300).into(contenidoViewHolder.imageView);
        contenidoViewHolder.tvOverview.setText(sPopular.getOverview());
    }

    @Override
    public int getItemCount() {
        return this.seriesTopRatedSearch.size();
    }

    @Override
    public Object obtenerValorOpcion(int posicion) {
        return this.seriesTopRatedSearch.get(posicion);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<STopRated> peliculasPopularesFilter = new ArrayList<STopRated>();
                if(constraint == null || constraint.length() == 0)
                    peliculasPopularesFilter.addAll(new ArrayList<STopRated>(seriesTopRatedFullList));
                else{
                    String filterPattern = constraint.toString().toLowerCase().trim();
                    for(STopRated sPopular : seriesTopRatedFullList){
                        if(sPopular.getName().toLowerCase().contains(filterPattern.toLowerCase())){
                            peliculasPopularesFilter.add(sPopular);
                        }
                    }
                }
                FilterResults results = new FilterResults();
                results.values = peliculasPopularesFilter;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                seriesTopRatedSearch.clear();
                seriesTopRatedSearch.addAll((List)results.values);
                notifyDataSetChanged();
            }
        };
    }
}
