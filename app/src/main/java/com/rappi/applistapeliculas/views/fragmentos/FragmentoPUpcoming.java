package com.rappi.applistapeliculas.views.fragmentos;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.rappi.applistapeliculas.R;
import com.rappi.applistapeliculas.models.pelicula.PUpComing;
import com.rappi.applistapeliculas.presenters.asynctask.CargadorDataAPI;
import com.rappi.applistapeliculas.views.adapters.RVAdapterPUpComing;

import java.util.List;

public class FragmentoPUpcoming extends FragmentoGenerico {

    List<PUpComing> listaPeliculasUpComing;

    public FragmentoPUpcoming(){
        super();
    }

    @Override
    public void setLista(List lista) {
        this.listaPeliculasUpComing = lista;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragmento_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.titlePeliculasUpcoming);
        CargadorDataAPI cargadorDataAPI = new CargadorDataAPI();
        cargadorDataAPI.setContext(getActivity());
        cargadorDataAPI.execute(this);
    }


    @Override
    public void cargarRV() {
        ProgressBar progressBar = (ProgressBar) getActivity().findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        setRvView((RecyclerView) getActivity().findViewById(R.id.rv));
        setRvAdapter(new RVAdapterPUpComing());
        getRvAdapter().cargarData(listaPeliculasUpComing);
        getRvAdapter().notifyDataSetChanged();
        getRvView().setLayoutManager(new LinearLayoutManager(getActivity()));
    }
}
