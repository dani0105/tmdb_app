package com.rappi.applistapeliculas.views.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.rappi.applistapeliculas.R;

import java.util.List;

public abstract class RVAdapter extends RecyclerView.Adapter<RVAdapter.ContenidoViewHolder> implements Filterable{

    public  RVAdapter(){

    }

    public abstract void clearContenido();


    public abstract void cargarData(List lista);
    @NonNull
    @Override
    public ContenidoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview, viewGroup, false);
        ContenidoViewHolder cvh = new ContenidoViewHolder(v);
        return cvh;
    }

    @Override
    public void onBindViewHolder(@NonNull ContenidoViewHolder contenidoViewHolder, int position){

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public Object obtenerValorOpcion(int posicion) {
        return null;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }



    public class ContenidoViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView tvTitle;
        TextView tvReleaseDate;
        TextView tvOverview;

        public ContenidoViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView)itemView.findViewById(R.id.ivMoviePhoto);
            tvTitle = (TextView)itemView.findViewById(R.id.tvTitle);
            tvReleaseDate = (TextView)itemView.findViewById(R.id.tvReleaseDate);
            tvOverview = (TextView)itemView.findViewById(R.id.tvOverview);

        }

    }

}
