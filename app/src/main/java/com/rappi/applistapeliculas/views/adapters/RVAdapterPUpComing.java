package com.rappi.applistapeliculas.views.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.Filter;

import com.rappi.applistapeliculas.R;
import com.rappi.applistapeliculas.models.pelicula.PUpComing;
import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RVAdapterPUpComing extends RVAdapter {

    List<PUpComing> peliculasUpComingFullList;
    List<PUpComing> peliculasUpComingSearch;

    public RVAdapterPUpComing() {
        super();

     }

    @Override
    public void clearContenido() {
        this.peliculasUpComingFullList.clear();
        this.peliculasUpComingSearch.clear();
    }

    @Override
    public void cargarData(List lista) {
        this.peliculasUpComingFullList = lista;
        this.peliculasUpComingSearch = new ArrayList<PUpComing>(peliculasUpComingFullList);
    }

    @NonNull
    @Override
    public ContenidoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return super.onCreateViewHolder(viewGroup, i);
    }

    @Override
    public void onBindViewHolder(@NonNull ContenidoViewHolder contenidoViewHolder, int position) {
        super.onBindViewHolder(contenidoViewHolder, position);
        PUpComing pUpComing = this.peliculasUpComingSearch.get(position);
        contenidoViewHolder.tvTitle.setText(pUpComing.getTitle());
        contenidoViewHolder.imageView.setImageResource(R.drawable.tmbd_iv);
        contenidoViewHolder.tvReleaseDate.setText(pUpComing.getRelease_date());
        Picasso.get().load(ConstantesPeliculasYSeries.IMAGE_URL
                + pUpComing.getPoster_path()).resize(400,300).into(contenidoViewHolder.imageView);
        contenidoViewHolder.tvOverview.setText(pUpComing.getOverview());
    }

    @Override
    public int getItemCount() {
        return this.peliculasUpComingSearch.size();
    }

    @Override
    public Object obtenerValorOpcion(int posicion) {
        return this.peliculasUpComingSearch.get(posicion);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<PUpComing> peliculasUpComingesFilter = new ArrayList<PUpComing>();
                if(constraint == null || constraint.length() == 0)
                    peliculasUpComingesFilter.addAll(new ArrayList<PUpComing>(peliculasUpComingFullList));
                else{
                    String filterPattern = constraint.toString().toLowerCase().trim();
                    for(PUpComing pUpComing : peliculasUpComingFullList){
                        if(pUpComing.getTitle().toLowerCase().contains(filterPattern.toLowerCase())){
                            peliculasUpComingesFilter.add(pUpComing);
                        }
                    }
                }
                FilterResults results = new FilterResults();
                results.values = peliculasUpComingesFilter;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                peliculasUpComingSearch.clear();
                peliculasUpComingSearch.addAll((List)results.values);
                notifyDataSetChanged();
            }
        };
    }
}
