package com.rappi.applistapeliculas.views.fragmentos;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.rappi.applistapeliculas.utils.exceptions.CustomException;
import com.rappi.applistapeliculas.views.adapters.RVAdapter;

import java.util.List;

public abstract class FragmentoGenerico extends Fragment implements Filterable {
    private RecyclerView rvView;
    private RVAdapter rvAdapter;

    public FragmentoGenerico(){

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                return null;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

            }
        };
    }

    public RecyclerView getRvView() {
        return rvView;
    }

    public void setRvView(RecyclerView rvView) {
        this.rvView = rvView;
        this.rvView.setAdapter(this.rvAdapter);
    }

    public RVAdapter getRvAdapter() {
        return rvAdapter;
    }

    public void setRvAdapter(RVAdapter rvAdapter) {
        this.rvAdapter = rvAdapter;
        this.rvView.setAdapter(this.rvAdapter);
    }

    public abstract void setLista(List lista);

    public abstract void cargarRV();

    public void showMessageError(CustomException e) {
        Toast.makeText(getActivity(), e.imprimirMensajeDeError(), Toast.LENGTH_SHORT).show();
    }
}
