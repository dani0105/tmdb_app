package com.rappi.applistapeliculas.models;

import com.rappi.applistapeliculas.models.pelicula.PPopular;
import com.rappi.applistapeliculas.models.pelicula.PTopRated;
import com.rappi.applistapeliculas.models.pelicula.PUpComing;
import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;
import com.rappi.applistapeliculas.utils.exceptions.PeliculaEnListaNoValida;

import java.util.ArrayList;
import java.util.List;

public class ListasPeliculas {

    private List<PPopular> peliculasPopular;
    private List<PTopRated> peliculasTopRated;
    private List<PUpComing> peliculasUpcoming;
    private static ListasPeliculas instancia;

    private ListasPeliculas(){
        this.peliculasPopular = new ArrayList<PPopular>();
        this.peliculasTopRated = new ArrayList<PTopRated>();
        this.peliculasUpcoming = new ArrayList<PUpComing>();
    }

    public static synchronized ListasPeliculas getInstancia(){
        if(instancia == null)
            instancia = new ListasPeliculas();

        return instancia;
    }

    public List<PPopular> getPeliculasPopular() {
        return peliculasPopular;
    }

    public List<PTopRated> getPeliculasTopRated() {
        return peliculasTopRated;
    }

    public List<PUpComing> getPeliculasUpcoming() {
        return peliculasUpcoming;
    }

    public void addNuevaPeliculaPopular(PPopular pPopular) throws PeliculaEnListaNoValida {
        if(pPopular.getCategoria() == ConstantesPeliculasYSeries.PELICULA_POPULAR)
            peliculasPopular.add(pPopular);
        else
            throw new PeliculaEnListaNoValida();
    }

    public void addNuevaPeliculaTopRated(PTopRated pTopRated) throws PeliculaEnListaNoValida {
        if(pTopRated.getCategoria() == ConstantesPeliculasYSeries.PELICULA_TOP_RATED)
            peliculasTopRated.add(pTopRated);
        else
            throw new PeliculaEnListaNoValida();
    }

    public void addNuevaPeliculaUpcoming(PUpComing pUpComing) throws PeliculaEnListaNoValida {
        if(pUpComing.getCategoria() == ConstantesPeliculasYSeries.PELICULA_UPCOMING)
            peliculasUpcoming.add(pUpComing);
        else
            throw new PeliculaEnListaNoValida();
    }
}
