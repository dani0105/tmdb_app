package com.rappi.applistapeliculas.models.pelicula;

import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;
import java.io.Serializable;

public class PPopular extends Pelicula implements Serializable {

    private static final long serialVersionUID = 1L;

    public PPopular(){
        super();
        setCategoria(ConstantesPeliculasYSeries.PELICULA_POPULAR);
    }
}
