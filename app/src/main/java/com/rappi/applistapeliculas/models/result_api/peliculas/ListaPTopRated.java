package com.rappi.applistapeliculas.models.result_api.peliculas;
import com.rappi.applistapeliculas.models.pelicula.PTopRated;

import java.util.ArrayList;
import java.util.List;

public class ListaPTopRated {
    List<PTopRated> results = new ArrayList<PTopRated>();

    public List<PTopRated> getResults() {
        return results;
    }

    public void setResults(List<PTopRated> results) {
        this.results = results;
    }
}
