package com.rappi.applistapeliculas.models.pelicula;

import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;

import java.io.Serializable;

public class PTopRated extends Pelicula implements Serializable{

    public PTopRated(){
        super();
        setCategoria(ConstantesPeliculasYSeries.PELICULA_TOP_RATED);
    }
}
