package com.rappi.applistapeliculas.models.result_api.peliculas;

import com.rappi.applistapeliculas.models.pelicula.PPopular;

import java.util.ArrayList;
import java.util.List;

public class ListaPPopulares {

    List<PPopular> results = new ArrayList<PPopular>();

    public List<PPopular> getResults() {
        return results;
    }

    public void setResults(List<PPopular> results) {
        this.results = results;
    }
}
