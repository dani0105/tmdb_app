package com.rappi.applistapeliculas.models.serie;

import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;

import java.io.Serializable;

public class SPopular extends Serie implements Serializable {
    public SPopular(){
        super();
        setCategoria(ConstantesPeliculasYSeries.SERIE_POPULAR);
    }
}
