package com.rappi.applistapeliculas.models.result_api.peliculas;

import com.rappi.applistapeliculas.models.pelicula.PUpComing;

import java.util.ArrayList;
import java.util.List;

public class ListaPUpcoming {
    List<PUpComing> results = new ArrayList<PUpComing>();

    public List<PUpComing> getResults() {
        return results;
    }

    public void setResults(List<PUpComing> results) {
        this.results = results;
    }
}
