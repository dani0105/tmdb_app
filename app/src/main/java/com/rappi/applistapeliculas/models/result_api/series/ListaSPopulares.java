package com.rappi.applistapeliculas.models.result_api.series;

import com.rappi.applistapeliculas.models.serie.SPopular;

import java.util.ArrayList;
import java.util.List;

public class ListaSPopulares {
    List<SPopular> results = new ArrayList<SPopular>();

    public List<SPopular> getResults() {
        return results;
    }

    public void setResults(List<SPopular> results) {
        this.results = results;
    }
}
