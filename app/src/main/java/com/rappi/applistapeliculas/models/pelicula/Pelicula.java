package com.rappi.applistapeliculas.models.pelicula;

import java.io.Serializable;
import java.util.List;

public abstract class Pelicula implements Serializable {

    private int id;
    private String title;
    private String release_date;

    private String poster_path;
    private List<Integer> genre_ids;
    private String overview;
    private int categoria; // Atributo no inicializado por Retrofit. Es interno de la aplicación

    public Pelicula() {
        this.id = 0;
        this.title = null;
        this.release_date = null;
        this.genre_ids = null;
        this.overview = null;
        this.categoria = 0;
        this.poster_path = null;
        this.genre_ids = null;
        this.overview = null;
        this.categoria = 0;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public List<Integer> getGenre_ids() {
        return genre_ids;
    }

    public void setGenre_ids(List<Integer> genre_ids) {
        this.genre_ids = genre_ids;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }
}
