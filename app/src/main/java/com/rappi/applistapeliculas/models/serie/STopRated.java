package com.rappi.applistapeliculas.models.serie;

import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;

import java.io.Serializable;

public class STopRated extends Serie implements Serializable {
    public STopRated(){
        super();
        setCategoria(ConstantesPeliculasYSeries.SERIE_TOP_RATED);
    }
}
