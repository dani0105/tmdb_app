package com.rappi.applistapeliculas.models.pelicula;


import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;
import java.io.Serializable;

public class PUpComing extends Pelicula implements Serializable {

    public PUpComing(){
        super();
        setCategoria(ConstantesPeliculasYSeries.PELICULA_UPCOMING);
    }
}
