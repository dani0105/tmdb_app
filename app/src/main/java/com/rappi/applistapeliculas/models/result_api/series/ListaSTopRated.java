package com.rappi.applistapeliculas.models.result_api.series;

import com.rappi.applistapeliculas.models.serie.STopRated;

import java.util.ArrayList;
import java.util.List;

public class ListaSTopRated {

    List<STopRated> results = new ArrayList<STopRated>();

    public List<STopRated> getResults() {
        return results;
    }

    public void setResults(List<STopRated> results) {
        this.results = results;
    }
}
