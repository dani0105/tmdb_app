package com.rappi.applistapeliculas.models.serie;


import java.io.Serializable;
import java.util.List;

public abstract class Serie implements Serializable {

    private int id;
    private String name;
    private String first_air_date ;
    private List<Integer> genre_ids;
    private String poster_path;
    private String overview;
    private int categoria; // Atributo no inicializado por Retrofit. Es interno de la aplicación

    public Serie() {
        this.id = 0;
        this.name = null;
        this.first_air_date = null;
        this.genre_ids = null;
        this.overview = null;
        this.categoria = 0;
        this.poster_path = null;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirst_air_date() {
        return first_air_date;
    }

    public void setFirst_air_date(String first_air_date) {
        this.first_air_date = first_air_date;
    }

    public List<Integer> getGenre_ids() {
        return genre_ids;
    }

    public void setGenre_ids(List<Integer> genre_ids) {
        this.genre_ids = genre_ids;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }
}
