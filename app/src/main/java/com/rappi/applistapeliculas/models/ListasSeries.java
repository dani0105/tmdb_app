package com.rappi.applistapeliculas.models;

import com.rappi.applistapeliculas.models.serie.SPopular;
import com.rappi.applistapeliculas.models.serie.STopRated;
import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;
import com.rappi.applistapeliculas.utils.exceptions.SerieEnListaNoValida;

import java.util.ArrayList;
import java.util.List;

public class ListasSeries {

    private List<SPopular> seriesPopular;
    private List<STopRated> seriesTopRated;
    private static ListasSeries instancia;

    private ListasSeries(){
        this.seriesPopular = new ArrayList<SPopular>();
        this.seriesTopRated = new ArrayList<STopRated>();
    }

    public static synchronized ListasSeries getInstancia(){
        if(instancia == null)
            instancia = new ListasSeries();

        return instancia;
    }

    public List<SPopular> getSeriesPopular() {
        return seriesPopular;
    }

    public List<STopRated> getSeriesTopRated() {
        return seriesTopRated;
    }

    public void addNuevaSeriePopular(SPopular sPopular) throws SerieEnListaNoValida {

        if(sPopular.getCategoria() == ConstantesPeliculasYSeries.SERIE_POPULAR)
            seriesPopular.add(sPopular);
        else
            throw new SerieEnListaNoValida();
    }

    public void addNuevaSerieTopRated(STopRated sTopRated) throws SerieEnListaNoValida {

        if(sTopRated.getCategoria() == ConstantesPeliculasYSeries.SERIE_TOP_RATED)
            seriesTopRated.add(sTopRated);
        else
            throw new SerieEnListaNoValida();
    }

}
