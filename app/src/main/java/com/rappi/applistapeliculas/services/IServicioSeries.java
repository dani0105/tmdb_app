package com.rappi.applistapeliculas.services;

import com.rappi.applistapeliculas.utils.exceptions.ConexionFallidaException;

public interface IServicioSeries {
    public void cargarListadoSeriesPopulares() throws ConexionFallidaException;

    public void cargarListadoSeriesTopRated() throws ConexionFallidaException;
}
