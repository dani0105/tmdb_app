package com.rappi.applistapeliculas.services.impl;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.rappi.applistapeliculas.models.ListasPeliculas;
import com.rappi.applistapeliculas.models.pelicula.PPopular;
import com.rappi.applistapeliculas.models.pelicula.PTopRated;
import com.rappi.applistapeliculas.models.pelicula.PUpComing;
import com.rappi.applistapeliculas.services.IServicioPeliculas;
import com.rappi.applistapeliculas.models.result_api.peliculas.ListaPPopulares;
import com.rappi.applistapeliculas.models.result_api.peliculas.ListaPTopRated;
import com.rappi.applistapeliculas.models.result_api.peliculas.ListaPUpcoming;
import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;
import com.rappi.applistapeliculas.utils.exceptions.ConexionFallidaException;
import com.rappi.applistapeliculas.utils.exceptions.PeliculaEnListaNoValida;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServicioPeliculas implements IServicioPeliculas {


    @Override
    public void cargarListadoPeliculasPopulares() throws ConexionFallidaException {
        final ListasPeliculas listasPeliculas = ListasPeliculas.getInstancia();
        Call<ListaPPopulares> peticionListaPopulares = ConstantesPeliculasYSeries.getRestClient().getPeliculasPopulares();

        peticionListaPopulares.enqueue(new Callback<ListaPPopulares>() {
            @Override
            public void onResponse(Call<ListaPPopulares> call, Response<ListaPPopulares> response) {
                switch (response.code()){
                    case 200:
                        try {
                            String bodyResponseToJson = new Gson().toJson(response.body());
                            JsonObject root = new JsonParser().parse((bodyResponseToJson)).getAsJsonObject();
                            JsonArray results = root.getAsJsonArray("results");
                            for(int i = 0; i < results.size(); i++){
                                JsonObject peliculaJSON = results.get(i).getAsJsonObject();
                                PPopular pPopular = new PPopular();
                                pPopular.setId(peliculaJSON.get("id").getAsInt());
                                pPopular.setTitle(peliculaJSON.get("title").getAsString());
                                pPopular.setOverview(peliculaJSON.get("overview").getAsString());
                                pPopular.setRelease_date(peliculaJSON.get("release_date").getAsString());
                                listasPeliculas.addNuevaPeliculaPopular(pPopular);
                            }

                        } /*catch (JsonException e) {
                            Log.d("Error Json respuesta", "Hubo un error procesando el JSON de la respuesta");
                        }*/ catch (PeliculaEnListaNoValida peliculaEnListaNoValida) {
                            peliculaEnListaNoValida.imprimirMensajeDeError();
                        }

                    break;

                }
            }

            @Override
            public void onFailure(Call<ListaPPopulares> call, Throwable t) {
                Log.e("error", t.toString());
            }
        });
    }

    @Override
    public void cargarListadoPeliculasTopRated() throws ConexionFallidaException {
        final ListasPeliculas listasPeliculas = ListasPeliculas.getInstancia();
        Call<ListaPTopRated> peticionListaTopRated = ConstantesPeliculasYSeries.getRestClient().getPeliculasTopRated();

        peticionListaTopRated.enqueue(new Callback<ListaPTopRated>() {
            @Override
            public void onResponse(Call<ListaPTopRated> call, Response<ListaPTopRated> response) {
                switch (response.code()){
                    case 200:
                        try {
                            String bodyResponseToJson = new Gson().toJson(response.body());
                            JsonObject root = new JsonParser().parse((bodyResponseToJson)).getAsJsonObject();
                            JsonArray results = root.getAsJsonArray("results");
                            for(int i = 0; i < results.size(); i++){
                                JsonObject peliculaJSON = results.get(i).getAsJsonObject();
                                PTopRated pTopRated = new PTopRated();
                                pTopRated.setId(peliculaJSON.get("id").getAsInt());
                                pTopRated.setTitle(peliculaJSON.get("title").getAsString());
                                pTopRated.setOverview(peliculaJSON.get("overview").getAsString());
                                pTopRated.setRelease_date(peliculaJSON.get("release_date").getAsString());
                                listasPeliculas.addNuevaPeliculaTopRated(pTopRated);
                            }

                        } /*catch (JsonException e) {
                            Log.d("Error Json respuesta", "Hubo un error procesando el JSON de la respuesta");
                        }*/ catch (PeliculaEnListaNoValida peliculaEnListaNoValida) {
                            peliculaEnListaNoValida.imprimirMensajeDeError();
                        }

                        break;

                }
            }

            @Override
            public void onFailure(Call<ListaPTopRated> call, Throwable t) {
                Log.e("error", t.toString());
            }
        });
    }

    @Override
    public void cargarListadoPeliculasUpcoming() throws ConexionFallidaException {
        final ListasPeliculas listasPeliculas = ListasPeliculas.getInstancia();
        Call<ListaPUpcoming> peticionListaUpcoming = ConstantesPeliculasYSeries.getRestClient().getPeliculasUpcoming();

        peticionListaUpcoming.enqueue(new Callback<ListaPUpcoming>() {
            @Override
            public void onResponse(Call<ListaPUpcoming> call, Response<ListaPUpcoming> response) {
                switch (response.code()){
                    case 200:
                        try {
                            String bodyResponseToJson = new Gson().toJson(response.body());
                            JsonObject root = new JsonParser().parse((bodyResponseToJson)).getAsJsonObject();
                            JsonArray results = root.getAsJsonArray("results");
                            for(int i = 0; i < results.size(); i++){
                                JsonObject peliculaJSON = results.get(i).getAsJsonObject();
                                PUpComing pUpComing = new PUpComing();
                                pUpComing.setId(peliculaJSON.get("id").getAsInt());
                                pUpComing.setTitle(peliculaJSON.get("title").getAsString());
                                pUpComing.setOverview(peliculaJSON.get("overview").getAsString());
                                pUpComing.setRelease_date(peliculaJSON.get("release_date").getAsString());
                                listasPeliculas.addNuevaPeliculaUpcoming(pUpComing);
                            }

                        } /*catch (JsonException e) {
                            Log.d("Error Json respuesta", "Hubo un error procesando el JSON de la respuesta");
                        }*/ catch (PeliculaEnListaNoValida peliculaEnListaNoValida) {
                            peliculaEnListaNoValida.imprimirMensajeDeError();
                        }

                        break;

                }
            }

            @Override
            public void onFailure(Call<ListaPUpcoming> call, Throwable t) {
                Log.e("error", t.toString());
            }
        });
    }

}
