package com.rappi.applistapeliculas.services.impl;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.rappi.applistapeliculas.models.ListasSeries;
import com.rappi.applistapeliculas.models.serie.SPopular;
import com.rappi.applistapeliculas.models.serie.STopRated;
import com.rappi.applistapeliculas.services.IServicioSeries;
import com.rappi.applistapeliculas.models.result_api.series.ListaSPopulares;
import com.rappi.applistapeliculas.models.result_api.series.ListaSTopRated;
import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;
import com.rappi.applistapeliculas.utils.exceptions.ConexionFallidaException;
import com.rappi.applistapeliculas.utils.exceptions.SerieEnListaNoValida;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServicioSeries implements IServicioSeries {

    @Override
    public void cargarListadoSeriesPopulares() throws ConexionFallidaException {
        final ListasSeries listasSeries = ListasSeries.getInstancia();
        Call<ListaSPopulares> peticionListaPopulares = ConstantesPeliculasYSeries.getRestClient().getSeriesPopulares();

        peticionListaPopulares.enqueue(new Callback<ListaSPopulares>() {
            @Override
            public void onResponse(Call<ListaSPopulares> call, Response<ListaSPopulares> response) {
                switch (response.code()){
                    case 200:
                        try {
                            String bodyResponseToJson = new Gson().toJson(response.body());
                            JsonObject root = new JsonParser().parse((bodyResponseToJson)).getAsJsonObject();
                            JsonArray results = root.getAsJsonArray("results");
                            for(int i = 0; i < results.size(); i++){
                                JsonObject peliculaJSON = results.get(i).getAsJsonObject();
                                SPopular sPopular = new SPopular();
                                sPopular.setId(peliculaJSON.get("id").getAsInt());
                                sPopular.setName(peliculaJSON.get("name").getAsString());
                                sPopular.setOverview(peliculaJSON.get("overview").getAsString());
                                sPopular.setFirst_air_date(peliculaJSON.get("first_air_date").getAsString());
                                listasSeries.addNuevaSeriePopular(sPopular);
                            }

                        } /*catch (JsonException e) {
                            Log.d("Error Json respuesta", "Hubo un error procesando el JSON de la respuesta");
                        }*/ catch (SerieEnListaNoValida serieEnListaNoValida) {
                            serieEnListaNoValida.imprimirMensajeDeError();
                        }

                        break;

                }
            }

            @Override
            public void onFailure(Call<ListaSPopulares> call, Throwable t) {
                Log.e("error", t.toString());
            }
        });

    }

    @Override
    public void cargarListadoSeriesTopRated() throws ConexionFallidaException {
        final ListasSeries listasSeries = ListasSeries.getInstancia();
        Call<ListaSTopRated> peticionListaPopulares = ConstantesPeliculasYSeries.getRestClient().getSeriesTopRated();

        peticionListaPopulares.enqueue(new Callback<ListaSTopRated>() {
            @Override
            public void onResponse(Call<ListaSTopRated> call, Response<ListaSTopRated> response) {
                switch (response.code()){
                    case 200:
                        try {
                            String bodyResponseToJson = new Gson().toJson(response.body());
                            JsonObject root = new JsonParser().parse((bodyResponseToJson)).getAsJsonObject();
                            JsonArray results = root.getAsJsonArray("results");
                            for(int i = 0; i < results.size(); i++){
                                JsonObject peliculaJSON = results.get(i).getAsJsonObject();
                                STopRated sTopRated = new STopRated();
                                sTopRated.setId(peliculaJSON.get("id").getAsInt());
                                sTopRated.setName(peliculaJSON.get("name").getAsString());
                                sTopRated.setOverview(peliculaJSON.get("overview").getAsString());
                                sTopRated.setFirst_air_date(peliculaJSON.get("first_air_date").getAsString());
                                listasSeries.addNuevaSerieTopRated(sTopRated);
                            }

                        } /*catch (JsonException e) {
                            Log.d("Error Json respuesta", "Hubo un error procesando el JSON de la respuesta");
                        }*/ catch (SerieEnListaNoValida serieEnListaNoValida) {
                            serieEnListaNoValida.imprimirMensajeDeError();
                        }

                        break;

                }
            }

            @Override
            public void onFailure(Call<ListaSTopRated> call, Throwable t) {
                Log.e("error", t.toString());
            }
        });
    }
}
