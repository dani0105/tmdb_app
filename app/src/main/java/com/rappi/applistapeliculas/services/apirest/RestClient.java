package com.rappi.applistapeliculas.services.apirest;

import com.rappi.applistapeliculas.models.result_api.peliculas.ListaPTopRated;
import com.rappi.applistapeliculas.models.result_api.peliculas.ListaPPopulares;
import com.rappi.applistapeliculas.models.result_api.peliculas.ListaPUpcoming;
import com.rappi.applistapeliculas.models.result_api.series.ListaSPopulares;
import com.rappi.applistapeliculas.models.result_api.series.ListaSTopRated;
import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RestClient {

    @GET("movie/popular?api_key=" + ConstantesPeliculasYSeries.API_KEY + "&language=es&page=1")
    Call<ListaPPopulares> getPeliculasPopulares();

    @GET("movie/top_rated?api_key=" + ConstantesPeliculasYSeries.API_KEY + "&language=es&page=1")
    Call<ListaPTopRated> getPeliculasTopRated();

    @GET("movie/upcoming?api_key=" + ConstantesPeliculasYSeries.API_KEY + "&language=es&page=1")
    Call<ListaPUpcoming> getPeliculasUpcoming();

    @GET("tv/popular?api_key=" + ConstantesPeliculasYSeries.API_KEY + "&language=es&page=1")
    Call<ListaSPopulares> getSeriesPopulares();

    @GET("tv/top_rated?api_key=" + ConstantesPeliculasYSeries.API_KEY + "&language=es&page=1")
    Call<ListaSTopRated> getSeriesTopRated();
}
