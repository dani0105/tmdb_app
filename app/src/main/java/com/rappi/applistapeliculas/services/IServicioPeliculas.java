package com.rappi.applistapeliculas.services;

import com.rappi.applistapeliculas.utils.exceptions.ConexionFallidaException;

public interface IServicioPeliculas {
    public void cargarListadoPeliculasPopulares() throws ConexionFallidaException;

    public void cargarListadoPeliculasTopRated() throws ConexionFallidaException;

    public void cargarListadoPeliculasUpcoming() throws ConexionFallidaException;
}
