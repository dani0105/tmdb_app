package com.rappi.applistapeliculas.utils.exceptions;

import android.util.Log;

public class PeliculaEnListaNoValida extends CustomException {

    @Override
    public String imprimirMensajeDeError() {
        setMensajeError("Esta intentando agregar una pelicula a una lista no valida");
        Log.e("Error agregacion", getMensajeError());
        return getMensajeError();
    }

}
