package com.rappi.applistapeliculas.utils;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rappi.applistapeliculas.services.apirest.RestClient;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConstantesPeliculasYSeries {


    public static final String URL = "https://api.themoviedb.org/3/";
    public static final int PELICULA_POPULAR = 1;
    public static final int PELICULA_TOP_RATED = 2;
    public static final int PELICULA_UPCOMING = 3;
    public static final int SERIE_POPULAR = 4;
    public static final int SERIE_TOP_RATED = 5;
    public static final String API_KEY = "265b9cbdbc9ac330ee151dfe12af01aa";

    public static final String PELICULA_POPULAR_FILENAME ="pp.ser";
    public static final String PELICULA_TOP_RATED_FILENAME ="ptr.ser";
    public static final String PELICULA_UPCOMING_FILENAME ="pup.ser";
    public static final String SERIE_POPULAR_FILENAME ="sp.ser";
    public static final String SERIE_TOP_RATED_FILENAME ="str.ser";
    public static final String IMAGE_URL = "https://image.tmdb.org/t/p/w185/";
    //public static final int PELICULA = 1;
    //public static final int SERIE = 2;

    private static RestClient instanciaRestClient;

    public synchronized static RestClient getRestClient() {

        if (instanciaRestClient == null) {
            Gson gson = new GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .create();

            instanciaRestClient = new Retrofit.Builder()
                    .baseUrl(ConstantesPeliculasYSeries.URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()
                    .create(RestClient.class);
        }

        return instanciaRestClient;
    }
}
