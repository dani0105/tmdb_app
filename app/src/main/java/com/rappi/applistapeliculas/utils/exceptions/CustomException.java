package com.rappi.applistapeliculas.utils.exceptions;

public abstract class CustomException extends Exception{

    private String mensajeError;

    public CustomException(){

    }

    public abstract String imprimirMensajeDeError();

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }
}
