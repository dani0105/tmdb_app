package com.rappi.applistapeliculas.utils.exceptions;

import android.util.Log;

public class TipoDeListaNoValidaException extends CustomException{
    @Override
    public String imprimirMensajeDeError() {
        setMensajeError("Tipo de lista no valida");
        Log.e("Error tipo de lista", getMensajeError());
        return getMensajeError();
    }
}
