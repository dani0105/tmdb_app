package com.rappi.applistapeliculas.utils.exceptions;

import android.util.Log;

public class ContextoNoInicializadoException extends CustomException {
    @Override
    public String imprimirMensajeDeError() {
        setMensajeError("No esta inicializado el contexto que necesita este objeto");
        Log.e("Error contexto", getMensajeError());
        return getMensajeError();

    }
}
