package com.rappi.applistapeliculas.utils.exceptions;

import android.util.Log;

public class ListaVaciaParaGuardarEnCacheException extends CustomException {
    @Override
    public String imprimirMensajeDeError() {
        setMensajeError("Lista descargada desde API vacía");
        Log.e("Error lista", getMensajeError());
        return getMensajeError();
    }
}
