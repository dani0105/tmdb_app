package com.rappi.applistapeliculas.utils.exceptions;

import android.util.Log;

public class ConexionFallidaException extends CustomException {

    @Override
    public String imprimirMensajeDeError() {
        setMensajeError("No se pudo conectar a la API de TheMovieDB");
        Log.e("Error conexión", getMensajeError());
        return getMensajeError();

    }
}
