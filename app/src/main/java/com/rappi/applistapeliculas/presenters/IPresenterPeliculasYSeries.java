package com.rappi.applistapeliculas.presenters;

import android.content.Context;
import com.rappi.applistapeliculas.models.pelicula.PPopular;
import com.rappi.applistapeliculas.models.pelicula.PTopRated;
import com.rappi.applistapeliculas.models.pelicula.PUpComing;
import com.rappi.applistapeliculas.models.serie.SPopular;
import com.rappi.applistapeliculas.models.serie.STopRated;
import com.rappi.applistapeliculas.utils.exceptions.ConexionFallidaException;
import com.rappi.applistapeliculas.utils.exceptions.ContextoNoInicializadoException;
import com.rappi.applistapeliculas.utils.exceptions.ListaVaciaParaGuardarEnCacheException;

import java.io.IOException;
import java.util.List;

public interface IPresenterPeliculasYSeries {

    public List<PPopular> getPeliculasPopularesDesdeLaAPIMadre() throws ConexionFallidaException, IOException;
    public List<PTopRated> getPeliculasTopRatedDesdeLaAPIMadre() throws ConexionFallidaException, IOException;
    public List<PUpComing> getPeliculasUpComingDesdeLaAPIMadre() throws ConexionFallidaException, IOException;
    public List<SPopular> getSeriesPopularesDesdeLaAPIMadre() throws ConexionFallidaException, IOException;
    public List<STopRated> getSeriesTopRatedDesdeLaAPIMadre() throws ConexionFallidaException, IOException;

    public List<PPopular> getPeliculasPopularesDesdeInternalStorage() throws IOException, ClassNotFoundException;
    public List<PTopRated> getPeliculasTopRatedDesdeInternalStorage() throws IOException, ClassNotFoundException;
    public List<PUpComing> getPeliculasUpComingDesdeInternalStorage() throws IOException, ClassNotFoundException;
    public List<SPopular> getSeriesPopularesDesdeInternalStorage() throws IOException, ClassNotFoundException;
    public List<PTopRated> getSeriesTopRatedDesdeInternalStorage() throws IOException, ClassNotFoundException;

    public List cargarDataDesdeCache( int tipodeContenido) throws IOException, ClassNotFoundException, ContextoNoInicializadoException;
    public void  guardarDataEnCache(List listaContenido) throws IOException, ContextoNoInicializadoException, ListaVaciaParaGuardarEnCacheException;
    void setContext(Context context);

}
