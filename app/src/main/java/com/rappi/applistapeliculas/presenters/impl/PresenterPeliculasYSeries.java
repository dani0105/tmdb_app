package com.rappi.applistapeliculas.presenters.impl;

import android.content.Context;

import com.rappi.applistapeliculas.models.ListasPeliculas;
import com.rappi.applistapeliculas.models.ListasSeries;
import com.rappi.applistapeliculas.models.pelicula.PPopular;
import com.rappi.applistapeliculas.models.pelicula.PTopRated;
import com.rappi.applistapeliculas.models.pelicula.PUpComing;
import com.rappi.applistapeliculas.models.serie.SPopular;
import com.rappi.applistapeliculas.models.serie.STopRated;
import com.rappi.applistapeliculas.presenters.IPresenterPeliculasYSeries;
import com.rappi.applistapeliculas.services.apirest.IServicioPeliculas;
import com.rappi.applistapeliculas.services.apirest.IServicioSeries;
import com.rappi.applistapeliculas.services.apirest.impl.ServicioPeliculas;
import com.rappi.applistapeliculas.services.apirest.impl.ServicioSeries;
import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;
import com.rappi.applistapeliculas.utils.exceptions.ConexionFallidaException;
import com.rappi.applistapeliculas.utils.exceptions.ContextoNoInicializadoException;
import com.rappi.applistapeliculas.utils.exceptions.ListaVaciaParaGuardarEnCacheException;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

public class PresenterPeliculasYSeries implements IPresenterPeliculasYSeries {

    private IServicioPeliculas servicioPeliculas;
    private IServicioSeries servicioSeries;
    private ListasPeliculas listasPeliculas;
    private ListasSeries listasSeries;
    private static PresenterPeliculasYSeries instancia;
    private ObjectInputStream ois;
    private Context context;

    private PresenterPeliculasYSeries(){
        this.servicioPeliculas = new ServicioPeliculas();
        this.servicioSeries = new ServicioSeries();
        this.listasPeliculas = ListasPeliculas.getInstancia();
        this.listasSeries = ListasSeries.getInstancia();
        this.context = null;
    }

    public synchronized static IPresenterPeliculasYSeries getInstancia() {
        if(instancia == null)
            instancia = new PresenterPeliculasYSeries();

        return instancia;
    }

    @Override
    public List<PPopular> getPeliculasPopularesDesdeLaAPIMadre() throws ConexionFallidaException, IOException {
        this.servicioPeliculas.cargarListadoPeliculasPopulares();
        List<PPopular> listadoPPopular = this.listasPeliculas.getPeliculasPopular();
        return listadoPPopular;
    }

    @Override
    public List<PTopRated> getPeliculasTopRatedDesdeLaAPIMadre() throws ConexionFallidaException, IOException {
        this.servicioPeliculas.cargarListadoPeliculasTopRated();
        List<PTopRated> listadoPTopRated = this.listasPeliculas.getPeliculasTopRated();
        return listadoPTopRated;
    }

    @Override
    public List<PUpComing> getPeliculasUpComingDesdeLaAPIMadre() throws ConexionFallidaException, IOException {
        this.servicioPeliculas.cargarListadoPeliculasUpcoming();
        List<PUpComing> listadoPUpcoming = this.listasPeliculas.getPeliculasUpcoming();
        return listadoPUpcoming;
    }

    @Override
    public List<SPopular> getSeriesPopularesDesdeLaAPIMadre() throws ConexionFallidaException, IOException {
        this.servicioSeries.cargarListadoSeriesPopulares();
        List<SPopular> listadoSPopular = this.listasSeries.getSeriesPopular();
        return listadoSPopular;
    }

    @Override
    public List<STopRated> getSeriesTopRatedDesdeLaAPIMadre() throws ConexionFallidaException, IOException {
        this.servicioSeries.cargarListadoSeriesTopRated();
        List<STopRated> listadoSTopRated = this.listasSeries.getSeriesTopRated();
        return listadoSTopRated;
    }

    //Internal storage
    @Override
    public List<PPopular> getPeliculasPopularesDesdeInternalStorage() throws IOException, ClassNotFoundException {

        try {
            return cargarDataDesdeCache(ConstantesPeliculasYSeries.PELICULA_POPULAR);
        } catch (ContextoNoInicializadoException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<PTopRated> getPeliculasTopRatedDesdeInternalStorage() throws IOException, ClassNotFoundException {

        try {
            return cargarDataDesdeCache(ConstantesPeliculasYSeries.PELICULA_TOP_RATED);
        } catch (ContextoNoInicializadoException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<PUpComing> getPeliculasUpComingDesdeInternalStorage() throws IOException, ClassNotFoundException {

        try {
            return cargarDataDesdeCache(ConstantesPeliculasYSeries.PELICULA_UPCOMING);
        } catch (ContextoNoInicializadoException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<SPopular> getSeriesPopularesDesdeInternalStorage() throws IOException, ClassNotFoundException {

        try {
            return cargarDataDesdeCache(ConstantesPeliculasYSeries.SERIE_POPULAR);
        } catch (ContextoNoInicializadoException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<PTopRated> getSeriesTopRatedDesdeInternalStorage() throws IOException, ClassNotFoundException {

        try {
            return cargarDataDesdeCache(ConstantesPeliculasYSeries.SERIE_TOP_RATED);
        } catch (ContextoNoInicializadoException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List cargarDataDesdeCache(int tipodeContenido) throws IOException, ClassNotFoundException, ContextoNoInicializadoException {

        if(context == null){
            throw new ContextoNoInicializadoException();
        }

        FileInputStream fis = null;
        ObjectInputStream ois = null;

        switch (tipodeContenido){
            case ConstantesPeliculasYSeries.PELICULA_POPULAR:
                fis = context.openFileInput(ConstantesPeliculasYSeries.PELICULA_POPULAR_FILENAME);
                ois = new ObjectInputStream(fis);
                List<PPopular> listaPPeliculas = (List<PPopular>) ois.readObject();
                fis.close();
                ois.close();
            return listaPPeliculas;

            case ConstantesPeliculasYSeries.PELICULA_TOP_RATED:
                fis = context.openFileInput(ConstantesPeliculasYSeries.PELICULA_TOP_RATED_FILENAME);
                ois = new ObjectInputStream(fis);
                List<PTopRated> listaTRPeliculas = (List<PTopRated>) ois.readObject();
                fis.close();
                ois.close();
            return listaTRPeliculas;

            case ConstantesPeliculasYSeries.PELICULA_UPCOMING:
                fis = context.openFileInput(ConstantesPeliculasYSeries.PELICULA_UPCOMING_FILENAME);
                ois = new ObjectInputStream(fis);
                List<PUpComing> listaUCPeliculas = (List<PUpComing>) ois.readObject();
                fis.close();
                ois.close();
            return listaUCPeliculas;


            case ConstantesPeliculasYSeries.SERIE_POPULAR:
                fis = context.openFileInput(ConstantesPeliculasYSeries.SERIE_POPULAR_FILENAME);
                ois = new ObjectInputStream(fis);
                List<SPopular> listaPoSeries = (List<SPopular>) ois.readObject();
                fis.close();
                ois.close();
            return listaPoSeries;

            case ConstantesPeliculasYSeries.SERIE_TOP_RATED:
                fis = context.openFileInput(ConstantesPeliculasYSeries.SERIE_TOP_RATED_FILENAME);
                ois = new ObjectInputStream(fis);
                List<STopRated> listaTRSeries = (List<STopRated>) ois.readObject();
                fis.close();
                ois.close();
            return listaTRSeries;

        }
        return null;
    }

    @Override
    public void guardarDataEnCache(List listaContenido) throws IOException, ContextoNoInicializadoException, ListaVaciaParaGuardarEnCacheException {

        if(context == null){
            throw new ContextoNoInicializadoException();
        }

        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        if((listaContenido != null) && (!listaContenido.isEmpty()) && (listaContenido.get(0) instanceof PPopular)){
            fos = context.openFileOutput(ConstantesPeliculasYSeries.PELICULA_POPULAR_FILENAME, Context.MODE_PRIVATE);
        }

        else if((listaContenido != null) && (!listaContenido.isEmpty()) && (listaContenido.get(0) instanceof PTopRated)){
            fos = context.openFileOutput(ConstantesPeliculasYSeries.PELICULA_TOP_RATED_FILENAME, Context.MODE_PRIVATE);
        }

        else if((listaContenido != null) && (!listaContenido.isEmpty()) && (listaContenido.get(0) instanceof PUpComing)){
            fos = context.openFileOutput(ConstantesPeliculasYSeries.PELICULA_UPCOMING_FILENAME, Context.MODE_PRIVATE);
        }

        else if((listaContenido != null) && (!listaContenido.isEmpty()) && (listaContenido.get(0) instanceof SPopular)){
            fos = context.openFileOutput(ConstantesPeliculasYSeries.SERIE_POPULAR_FILENAME, Context.MODE_PRIVATE);
        }

        else if((listaContenido != null) && (!listaContenido.isEmpty()) && (listaContenido.get(0) instanceof STopRated)){
            fos = context.openFileOutput(ConstantesPeliculasYSeries.SERIE_TOP_RATED_FILENAME, Context.MODE_PRIVATE);
        }

        else{
            throw new ListaVaciaParaGuardarEnCacheException();
        }

        if(fos != null){
            oos = new ObjectOutputStream(fos);
            oos.writeObject(listaContenido);
            oos.close();
            fos.close();
        }
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
