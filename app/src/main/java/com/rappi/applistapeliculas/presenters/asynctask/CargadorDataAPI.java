package com.rappi.applistapeliculas.presenters.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;

import com.rappi.applistapeliculas.models.pelicula.PPopular;
import com.rappi.applistapeliculas.models.pelicula.PTopRated;
import com.rappi.applistapeliculas.models.pelicula.PUpComing;
import com.rappi.applistapeliculas.models.serie.SPopular;
import com.rappi.applistapeliculas.models.serie.STopRated;
import com.rappi.applistapeliculas.presenters.IPresenterPeliculasYSeries;
import com.rappi.applistapeliculas.presenters.impl.PresenterPeliculasYSeries;
import com.rappi.applistapeliculas.utils.ConstantesPeliculasYSeries;
import com.rappi.applistapeliculas.utils.exceptions.ConexionFallidaException;
import com.rappi.applistapeliculas.utils.exceptions.ContextoNoInicializadoException;
import com.rappi.applistapeliculas.utils.exceptions.ListaVaciaParaGuardarEnCacheException;
import com.rappi.applistapeliculas.views.fragmentos.FragmentoGenerico;
import com.rappi.applistapeliculas.views.fragmentos.FragmentoPPopular;
import com.rappi.applistapeliculas.views.fragmentos.FragmentoPTopRated;
import com.rappi.applistapeliculas.views.fragmentos.FragmentoPUpcoming;
import com.rappi.applistapeliculas.views.fragmentos.FragmentoSPopular;
import com.rappi.applistapeliculas.views.fragmentos.FragmentoSTopRated;

import java.io.IOException;
import java.util.List;

public class CargadorDataAPI extends AsyncTask<FragmentoGenerico, Integer, List> {

    FragmentoGenerico fragmentoGenerico;
    IPresenterPeliculasYSeries presenterPeliculasYSeriesYSeries;
    private Context context;
    @Override
    protected List doInBackground(FragmentoGenerico... fragmentoGenericos) {
        fragmentoGenerico = fragmentoGenericos[0];
        presenterPeliculasYSeriesYSeries = PresenterPeliculasYSeries.getInstancia();
        presenterPeliculasYSeriesYSeries.setContext(context);
        if(fragmentoGenerico instanceof FragmentoPPopular){
            try {
                List<PPopular> peliculasPopulares = presenterPeliculasYSeriesYSeries.getPeliculasPopularesDesdeLaAPIMadre();
                Thread.sleep(5000);

                return peliculasPopulares;
            } catch (ConexionFallidaException e) {
                return null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(fragmentoGenerico instanceof FragmentoPTopRated){
            try {
                List<PTopRated> peliculasTopRated = presenterPeliculasYSeriesYSeries.getPeliculasTopRatedDesdeLaAPIMadre();
                Thread.sleep(5000);
                return peliculasTopRated;
            } catch (ConexionFallidaException e) {
                return null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(fragmentoGenerico instanceof FragmentoPUpcoming){
            try {
                List<PUpComing> peliculasUpcoming = presenterPeliculasYSeriesYSeries.getPeliculasUpComingDesdeLaAPIMadre();
                Thread.sleep(5000);
                return peliculasUpcoming;
            } catch (ConexionFallidaException e) {
                return null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(fragmentoGenerico instanceof FragmentoSPopular){
            try {
                List<SPopular> seriesPopulares = presenterPeliculasYSeriesYSeries.getSeriesPopularesDesdeLaAPIMadre();
                Thread.sleep(5000);
                return seriesPopulares;
            } catch (ConexionFallidaException e) {
                return null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(fragmentoGenerico instanceof FragmentoSTopRated){
            try {
                List<STopRated> seriesTopRated = presenterPeliculasYSeriesYSeries.getSeriesTopRatedDesdeLaAPIMadre();
                Thread.sleep(5000);
                return seriesTopRated;
            } catch (ConexionFallidaException e) {
                return null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(List list) {
        super.onPostExecute(list);

        if ((list != null) && (!list.isEmpty())){
            try {
                presenterPeliculasYSeriesYSeries.guardarDataEnCache(list);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ContextoNoInicializadoException e) {
                e.printStackTrace();
                fragmentoGenerico.showMessageError(e);
            } catch (ListaVaciaParaGuardarEnCacheException e) {
                e.printStackTrace();
                fragmentoGenerico.showMessageError(e);
                //return;
            }
        }
        else{
            try {
                if(fragmentoGenerico instanceof FragmentoPPopular)
                list = presenterPeliculasYSeriesYSeries.cargarDataDesdeCache(ConstantesPeliculasYSeries.PELICULA_POPULAR);

                if(fragmentoGenerico instanceof FragmentoPTopRated)
                    list = presenterPeliculasYSeriesYSeries.cargarDataDesdeCache(ConstantesPeliculasYSeries.PELICULA_TOP_RATED);

                if(fragmentoGenerico instanceof FragmentoPUpcoming)
                    list = presenterPeliculasYSeriesYSeries.cargarDataDesdeCache(ConstantesPeliculasYSeries.PELICULA_UPCOMING);

                if(fragmentoGenerico instanceof FragmentoSPopular)
                    list = presenterPeliculasYSeriesYSeries.cargarDataDesdeCache(ConstantesPeliculasYSeries.SERIE_POPULAR);

                if(fragmentoGenerico instanceof FragmentoSTopRated)
                    list = presenterPeliculasYSeriesYSeries.cargarDataDesdeCache(ConstantesPeliculasYSeries.SERIE_TOP_RATED);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (ContextoNoInicializadoException e) {
                fragmentoGenerico.showMessageError(e);
                e.printStackTrace();
            }
        }

        fragmentoGenerico.setLista(list);
        fragmentoGenerico.cargarRV();
    }


    public void setContext(FragmentActivity activity) {
        this.context = activity;
    }
}
